FROM haproxy:latest
COPY haproxy.cfg /usr/local/etc/haproxy/haproxy.cfg
COPY error.http /usr/local/etc/haproxy/errors/error.http
